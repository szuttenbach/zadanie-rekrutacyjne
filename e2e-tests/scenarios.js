'use strict';

describe('users', function () {

    it('should automatically redirect to /user-list when location hash/fragment is empty', function () {
        browser.get('index.html');
        expect(browser.getLocationAbsUrl()).toMatch("/user-list");
    });

    beforeEach(function () {
        browser.get('index.html#/user-list');
    });

    describe('user-list', function () {

        it('should have table with 10 elements', function () {
            expect(element.all(by.repeater('user in userList')).count()).toEqual(10);
        });

        it('first name should be "Vilma"', function () {
            element(by.repeater('user in userList').row(0).column('user.name')).getText().then(function (val) {
                expect(val).toEqual("Vilma");
            });
        });

        it('should delete 2 elements from table', function () {

            element(by.repeater('user in userList').row(0)).$('.btn-delete').click();
            element(by.repeater('user in userList').row(0)).$('.btn-delete').click();

            expect(element.all(by.repeater('user in userList')).count()).toEqual(8);
        });

        it('after deletes two first elements name should be "Tammie"', function () {
            element(by.repeater('user in userList').row(0)).$('.btn-delete').click();
            element(by.repeater('user in userList').row(0)).$('.btn-delete').click();

            element(by.repeater('user in userList').row(0).column('user.name')).getText().then(function (val) {
                expect(val).toEqual("Tammie");
            });
        });

        it('edit first element name', function () {
            element(by.repeater('user in userList').row(0)).$('.btn-edit').click();
            element.all(by.model('user.name')).first().clear().sendKeys("Piotr");
            element(by.repeater('user in userList').row(0)).$('.btn-save').click();

            element(by.repeater('user in userList').row(0).column('user.name')).getText().then(function (val) {
                expect(val).toEqual("Piotr");
            });
        });

        it('edit more then one element name', function () {
            element(by.repeater('user in userList').row(0)).$('input[type=checkbox]').click();
            element(by.repeater('user in userList').row(1)).$('input[type=checkbox]').click();

            element(by.css('.edit-selected')).click();

            element.all(by.model('user.name')).get(0).clear().sendKeys("Piotr");
            element.all(by.model('user.name')).get(1).clear().sendKeys("Katarzyna");

            element(by.repeater('user in userList').row(0)).$('.btn-save').click();
            element(by.repeater('user in userList').row(1)).$('.btn-save').click();

            element.all(by.repeater('user in userList').column('user.name')).getText().then(function (values) {
                expect(values[0]).toEqual("Piotr");
                expect(values[1]).toEqual("Katarzyna");
            });
        });
    });
});