describe('Controller: UserListCtrl', function () {

    var $scope, ctrl, APIservice;
    var deferred;

    beforeEach(module('users.list'));

    beforeEach(inject(function ($q, $rootScope, $controller, $http) {
        $scope = $rootScope.$new();

        APIservice = {
            getUsers: function () {
                deferred = $q.defer();
                deferred.resolve({data: [{
                            "id": 1,
                            "name": "Vilma",
                            "surname": "Cross",
                            "date": "1982-09-11",
                            "mobile": "(965) 494-2534",
                            "address": "Savannah, Narrows Avenue, 21"
                        },
                        {
                            "id": 2,
                            "name": "Natalia",
                            "surname": "Marks",
                            "date": "1987-08-05",
                            "mobile": "(937) 429-2176",
                            "address": "Wanamie, Kings Hwy, 5"
                        }]});
                return deferred.promise;
            },
            edit: function (user) {
                deferred = $q.defer();
                deferred.resolve({data: {success: true}});
                return deferred.promise;
            },
            remove: function (user) {
                deferred = $q.defer();
                deferred.resolve({data: {success: true}});
                return deferred.promise;
            }
        };

        ctrl = $controller('UserListCtrl', {$scope: $scope, APIservice: APIservice});
    }));

    it('should call getUsers on the APIservice when getUserList is called', inject(function () {
        spyOn(APIservice, 'getUsers').and.callThrough();
        $scope.getUserList();
        expect(APIservice.getUsers).toHaveBeenCalled();
    }));

    it('should populate the userList when getUserList is called', inject(function () {
        $scope.getUserList();
        $scope.$root.$digest();
        expect($scope.userList.length).toBe(2);
    }));

    it('should call edit on the APIservice when saveUser is called', inject(function () {
        $scope.getUserList();
        $scope.$root.$digest();

        spyOn(APIservice, 'edit').and.callThrough();
        $scope.save(1);
        expect(APIservice.edit).toHaveBeenCalled();
    }));

    it('should populate the userList when getUserList is called', inject(function () {
        $scope.getUserList();
        $scope.$root.$digest();

        var result = $scope.saveUser(1);
        $scope.$root.$digest();
        expect(result).toBeDefined();
    }));

    it('should select users when checkAsSelected is called', inject(function () {
        $scope.getUserList();
        $scope.$root.$digest();

        $scope.checkAsSelected(1, true);
        expect($scope.selectedUsers.length).toBe(1);
    }));

    it('should populate the remove when deleteUser is called', inject(function () {
        $scope.getUserList();
        $scope.$root.$digest();

        spyOn(APIservice, 'remove').and.callThrough();
        $scope.deleteUser(1);
        expect(APIservice.remove).toHaveBeenCalled();
    }));

    it('should populate the remove when deleteUser is called', inject(function () {
        var userId = 1, result;

        $scope.getUserList();
        $scope.$root.$digest();

        angular.forEach($scope.userList, function (user) {
            if (parseInt(user.id) === parseInt(userId)) {
                result = $scope.deleteUser(user);
            }
        });

        $scope.$root.$digest();
        expect($scope.userList.length).toBe(1);
    }));
});