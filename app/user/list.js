'use strict';

angular.module('users.list', ['ngRoute'])

        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/user-list', {
                    templateUrl: 'user/list.html',
                    controller: 'UserListCtrl'
                });
            }])

        .controller('UserListCtrl', ['$scope', 'APIservice', '$q', '$timeout', function ($scope, APIservice, $q, $timeout) {
                /*
                 * initialize userList array
                 */
                $scope.userList = [];

                /*
                 * additional array to keep selected id's
                 */
                $scope.selectedUsers = [];

                /*
                 * method for getting list of users from APIservice
                 */
                $scope.getUserList = function () {
                    APIservice.getUsers().then(function successCallback(response) {
                        if (response.data) {
                            $scope.userList = response.data;
                        }
                    });
                };

                /**
                 * initial get user list
                 */
                $scope.getUserList();

                /*
                 * method for making fields editable
                 */
                $scope.editSelected = function () {
                    if (!$scope.selectedUsers.length > 0) {
                        $('.error-selected').show();
                        $timeout(function () {
                            $('.error-selected').hide();
                        }, 1500);

                        return;
                    }

                    angular.forEach($scope.selectedUsers, function (userId) {
                        $scope.editUser(userId);
                    });
                };


                /*
                 * make editable row for selected user
                 */
                $scope.editUser = function (userId) {
                    angular.forEach($scope.userList, function (user) {
                        if (parseInt(user.id) === parseInt(userId)) {
                            user.edit = true;
                        }
                    });
                };

                /*
                 * Method to edit user to database
                 * @param {integer} userId
                 * @returns {$q@call;defer.promise}
                 */
                $scope.saveUser = function (userId) {
                    var deferred = $q.defer();

                    angular.forEach($scope.userList, function (user) {
                        if (parseInt(user.id) === parseInt(userId)) {
                            APIservice.edit(user).then(function successCallback(response) {
                                if (response.data.success) {
                                    deferred.resolve({user: user, success: true});
                                } else {
                                    deferred.resolve({user: user, success: false});
                                }
                            });
                        }
                    });

                    return deferred.promise;
                };

                /**
                 * method to save user
                 * @param {integer} userId
                 * @returns {boolean}
                 */
                $scope.save = function (userId) {
                    $scope.saveUser(userId).then(function (results) {
                        if (results.success) {
                            results.user.edit = false;
                            return true;
                        }
                    });
                };

                /**
                 * remove user method
                 * @param {object} user
                 * @returns {boolean}
                 */
                $scope.deleteUser = function (user) {
                    APIservice.remove(user).then(function successCallback(response) {
                        if (response.data.success) {
                            var index = $scope.userList.indexOf(user);
                            if (index > -1) {
                                $scope.userList.splice(index, 1);
                                return true;
                            }
                        }
                    });
                };

                /*
                 * method for selecting user and saving them in additional selectedUsers array
                 */
                $scope.checkAsSelected = function (id, checked) {
                    if (checked) {
                        $scope.selectedUsers.push(id);
                    } else {
                        var index = $.inArray(id, $scope.selectedUsers);

                        if (index > -1) {
                            $scope.selectedUsers.splice(index, 1);
                        }
                    }
                };
            }]);