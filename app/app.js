'use strict';
// Declare app level module which depends on views, and components
angular.module('users', [
    'ngRoute',
    'users.services',
    'users.list',
    'ngMockE2E'
])

        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.otherwise({redirectTo: '/user-list'});
            }])

        .run(['$httpBackend', '$http', 'APIURL', function ($httpBackend, $http, APIURL) {

                var users = $http({
                    method: 'GET',
                    url: '/users.json'
                }).then(function (response) {
                    // returns the current list of user list
                    $httpBackend.whenGET('/users').respond(response.data);
                });

                // edit user item
                $httpBackend.whenPOST(new RegExp(APIURL + '/edit/\\d+')).respond(function (method, url, data) {
                    return [200, {success: true}, {}];
                });

                // remove a user from the users array
                $httpBackend.whenPOST(new RegExp(APIURL + '/remove/\\d+')).respond(function (method, url, data) {
                    return [200, {success: true}, {}];
                });

                $httpBackend.whenGET('/users.json').passThrough();
                $httpBackend.whenGET(new RegExp('\/.*html')).passThrough();
            }]);
