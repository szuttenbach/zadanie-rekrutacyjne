'use strict';

angular.module('users.services', [])

        .constant('APIURL', 'http://users.impaqgroup.com')

        /*
         * Factory to retrieve data from API 
         * @param {promise} $http
         * @param {string} APIURL
         * @returns {APIservice}
         */
        .factory('APIservice', ['$http', 'APIURL', function ($http, APIURL) {
                var API = {};

                /**
                 * get users method
                 * @returns {promise}
                 */
                API.getUsers = function () {
                    return $http({
                        method: 'GET',
                        url: '/users'
                    });
                };

                /**
                 * get user method
                 * @param {object} user
                 * @returns {promise}
                 */
                API.find = function (user) {
                    return $http({
                        method: 'GET',
                        url: APIURL + '/find/' + parseInt(user.id)
                    });
                };

                /**
                 * get users method by ids
                 * @param {array} ids
                 * @returns {promise}
                 */
                API.findAll = function (ids) {
                    if (ids instanceof Array) {
                        return $http({
                            method: 'GET',
                            url: APIURL + '/findall/' + ids.toString()
                        });
                    }
                };

                /**
                 * removes user
                 * @param {object} user
                 * @returns {promise}
                 */
                API.remove = function (user) {
                    return $http({
                        method: 'POST',
                        url: APIURL + '/remove/' + parseInt(user.id)
                    });
                };

                /**
                 * edits user
                 * @param {object} user
                 * @returns {promise}
                 */
                API.edit = function (user) {
                    return $http({
                        method: 'POST',
                        url: APIURL + '/edit/' + parseInt(user.id),
                        data: {user: user}
                    });
                };

                /*
                 * return APIservice service
                 */
                return API;

            }]);