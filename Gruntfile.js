module.exports = function (grunt) {

    grunt.initConfig({
        cssmin: {
            target: {
                files: {
                    'build/css/output.css': [
                        'app/bower_components/bootstrap/dist/css/bootstrap.css',
                        'app/app.css'
                    ]
                }
            }
        },
        copy: {
            index: {
                src: 'app/index_prod.html',
                dest: 'build/index.html'
            },
            templates: {
                src: 'app/user/list.html',
                dest: 'build/user/list.html'
            },
            fonts: {
                expand: true,
                dot: true,
                cwd: 'app/bower_components/bootstrap',
                dest: 'build/',
                src: [
                    'fonts/*.*'
                ]
            },
            users: {
                src: 'app/users.json',
                dest: 'build/users.json'
            }
        },
        uglify: {
            libs: {
                files: {
                    'build/js/libs.min.js': [
                        'app/bower_components/angular/angular.js',
                        'app/bower_components/angular-route/angular-route.js',
                        'app/bower_components/angular-mocks/angular-mocks.js',
                        'app/bower_components/jquery/dist/jquery.js',
                        'app/bower_components/bootstrap/dist/js/bootstrap.js'
                    ]
                }
            },
            app: {
                files: {
                    'build/js/app.min.js': [
                        'app/app.js',
                        'app/services/APIservice.js',
                        'app/user/list.js'
                    ]
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('css', ['cssmin:target']);
    grunt.registerTask('copyfiles', ['copy:index', 'copy:templates', 'copy:fonts', 'copy:users']);
    grunt.registerTask('compressjs', ['uglify:libs', 'uglify:app']);

    grunt.registerTask('release', ['css', 'copyfiles', 'compressjs']);
};