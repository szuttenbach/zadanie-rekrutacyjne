After downloading repository please in main folder execute commands:

npm install
bower update
grunt release

[for production env]
npm start

[for debug env and tests]
npm run start-local (http://localhost:8000/)
npm run test
npm run protractor
